function scale = findScale(pts)

    
    
    c = mean(pts(1:2,:)')';            % Centroid of finite points
    newp(1,:) = pts(1,:)-c(1); % Shift origin to centroid.
    newp(2,:) = pts(2,:)-c(2);
    
    dist = sqrt(newp(1,:).^2 + newp(2,:).^2);
    meandist = mean(dist(:));  
    
    scale = sqrt(2)/meandist;
    
%     T = [scale   0   -scale*c(1)
%          0     scale -scale*c(2)
%          0       0      1      ];
%     
%      
% %    T = eye(3);
%      
%     newpts = T*pts;
%     
%     
end