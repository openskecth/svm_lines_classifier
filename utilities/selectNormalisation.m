function selectNormalisation(brightness, indConstruction, indDescriptive)
    
    mean_vals_sketch_construction = normalizeMax(brightness, indConstruction);
    mean_vals_sketch_descriptive  = normalizeMax(brightness, indDescriptive);
    
    std_norm =[];
    mean_norm=[];
    j = 1;
    
    [std_norm, mean_norm] = normiliseAndAssign(mean_vals_sketch_construction,mean_vals_sketch_descriptive,...
        j,std_norm, mean_norm);    
    strings{1} = 'max';
    
    for j = 2:15
        mean_vals_sketch_construction = normalizePrctile(brightness, indConstruction, 101-j);        
        mean_vals_sketch_descriptive = normalizePrctile(brightness, indDescriptive, 101-j);
        
        [std_norm, mean_norm] = normiliseAndAssign(mean_vals_sketch_construction,mean_vals_sketch_descriptive,...
            j,std_norm, mean_norm);    
        
        strings{j} = sprintf('%d', 101-j);        
    end
    
    mean_vals_sketch_construction = noNorm(brightness,indConstruction);    
    mean_vals_sketch_descriptive = noNorm(brightness, indDescriptive);
    [std_norm, mean_norm] = normiliseAndAssign(mean_vals_sketch_construction,mean_vals_sketch_descriptive,...
            j+1,std_norm, mean_norm);       
    strings{j+1} = 'no';
    
    sum_stds_groups = sum(std_norm,2);
    [~, ind] = min(sum_stds_groups);
    
    [~, ind_sort] = sort(sum_stds_groups);
    strings(ind_sort)
    tt = max(mean_norm, [], 2)./min(mean_norm, [], 2);
    [tt_sorted, ind_sort_ratios] = sort(tt, 'descend');
    strings(ind_sort_ratios)
    
end

function [std_norm, mean_norm] = normiliseAndAssign(mean_vals_sketch_construction,mean_vals_sketch_descriptive,...
    j,std_norm, mean_norm)
    
    norm_factor = max([mean_vals_sketch_descriptive; mean_vals_sketch_descriptive]);
     
    mean_vals_sketch_construction = mean_vals_sketch_construction/norm_factor;    
    mean_vals_sketch_descriptive  = mean_vals_sketch_descriptive/norm_factor;
    
    std_norm(j,1) = std(mean_vals_sketch_construction);
    mean_norm(j,1) = mean(mean_vals_sketch_construction);
   
    std_norm(j,2) = std(mean_vals_sketch_descriptive);
    mean_norm(j,2) = mean(mean_vals_sketch_descriptive);
end


function mean_val = noNorm(vals, ind)
    mean_val = zeros(size(vals,1), 1);
    for i = 1:size(vals,1)         
        mean_val(i) = mean(vals{i}(ind{i}));
    end
%     mean_val = mean_val;
end

function mean_val = normalizeMax(vals,ind)
    mean_val = zeros(size(vals,1), 1);
    for i = 1:size(vals,1)
        temp = vals{i}(ind{i})/max(vals{i});
        mean_val(i) = mean(temp);
    end
%     mean_val = mean_vall);
end


function mean_val = normalizePrctile(vals,ind, prctval)
    mean_val = zeros(size(vals,1), 1);
    for i = 1:size(vals,1)
        p = prctile(vals{i}, prctval);
        temp = vals{i}(ind{i})/p;
        mean_val(i) = mean(temp);
    end
%     mean_val = mean_val/max(mean_val);
end
