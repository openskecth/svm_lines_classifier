function [brightness_mean_vals, ...
    times_mean_vals, ...    
    stroke_numbers_mean_vals, ...
    lengths_mean_vals, ...
    curvatures_mean_vals, ...
    speeds_mean_vals...
    ] = visualizeStatisticsGroups(statistics, indConstruction, indDescriptive)

    fig_num = 10;
    brightness_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.brightness,  'brightness', ...
        indConstruction, indDescriptive);
    
    fig_num = fig_num + 1;
    times_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.times,  'times', ...
        indConstruction, indDescriptive);
    
    fig_num = fig_num + 1;
    stroke_numbers_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.stroke_numbers,  'stroke numbers', ...
        indConstruction, indDescriptive);
    
    fig_num = fig_num + 1;
    lengths_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.lengths,  'lengths', ...
        indConstruction, indDescriptive);
    
    fig_num = fig_num + 1;
    speeds_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.speeds,  'speeds', ...
        indConstruction, indDescriptive);
    
    fig_num = fig_num + 1;
    curvatures_mean_vals = plotHistAndMeanGroups(fig_num, ...
        statistics.curvatures_v3,  'curvatures', ...
        indConstruction, indDescriptive);
end

function mean_vals = plotHistAndMeanGroups(fig_num, vals, xlalbel_str, indConstruction, indDescriptive)
%     figure;
%     hold on;
    total_num_strokes = length(vals);
    
    [legend_str_constrcution, mean_vals(1)] = ...
        plotHistAndMean(vals(indConstruction), total_num_strokes, 'm');
    [legend_str_descriptive, mean_vals(2)] = ...
        plotHistAndMean(vals(indDescriptive), total_num_strokes, 'c');
    
%     legend('Construction', legend_str_constrcution, 'Descriptive', legend_str_descriptive);
    
%     xlabel(xlalbel_str);
end

function [legend_str, mean_vals] = plotHistAndMean(vals, total_num_strokes, color_str)

    [counts,centers] = hist(vals, 100);    
    counts = counts/total_num_strokes;    
%     bar(centers, counts, 'EdgeColor', 'none', 'FaceColor', color_str);    
    mean_vals = mean(vals);
    std_vals = std(vals);
%     plot([mean_vals mean_vals], [0, max(counts)], [':' color_str]);
    legend_str = sprintf('mean %.2f, std %.2f', mean_vals, std_vals);
end