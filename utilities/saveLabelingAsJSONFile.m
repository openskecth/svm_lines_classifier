function saveLabelingAsJSONFile(filename, labels)
    fid = fopen(filename,'w');
    
    
    class = {'construction'; 'descriptive'};
    value = [-1;1];    
    ldata.info = table(class, value);
    ldata.labels_strokes = labels;
    str = jsonencode(ldata);
    fwrite(fid, str);
    fclose(fid);
end

