function save_as_svg_line_type(strokes, h, filepath, lines_str_number)

scale = 1.0;
pen_width = 1.5*scale;

fid = fopen(filepath, 'w');

str = sprintf('<?xml version="1.0" encoding="utf-8" ?>\n'); 
fwrite(fid, str);
str = sprintf('<svg baseProfile="full" height="%d" version="1.1" viewBox="0,0,%d,%d" width="%d" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css"><![CDATA[\n', h,h,h,h);
fwrite(fid, str);
str = sprintf('\t.background { fill: white; }\n');
fwrite(fid, str);
str = sprintf('\t.line { stroke: firebrick; stroke-width: .1mm; }\n');
fwrite(fid, str);
str = sprintf('\t.blacksquare { fill: indigo; }\n');
fwrite(fid, str);
str = sprintf('\t.whitesquare { fill: white; }\n');
fwrite(fid, str);
str = sprintf(']]></style></defs>\n');
fwrite(fid, str);
% colors = uint8(colormap(parula(length(strokes)))*255);
colors = uint8(zeros(length(strokes),3)*255);


for i=1:length(strokes)   
%    if ~ismember(strokesType(i), [contour, key])         
%        continue;
%    end
   
%    avg_pressure = mean(cat(1, strokes(i).points(:).p));
    if length(strokes(i).points) < 2
        try
            avg_pressure = mean(cat(1, strokes(i).points(:).p));
        catch
            avg_pressure = 0.0;
            disp('');
        end
    else    
        for j = 1:(length(strokes(i).points)-1)
            w(j) = sqrt((strokes(i).points(j+1).x-strokes(i).points(j).x).^2 + (strokes(i).points(j+1).y-strokes(i).points(j).y).^2);
            p(j) = strokes(i).points(j+1).p;%0.5*(strokes(i).points(j+1).p + strokes(i).points(j).p);
        end

        avg_pressure = sum(p.*w)./sum(w);
    end
   if avg_pressure < 1e-19
       fprintf('Zero pressure stroke %d\n', i);
       continue;       
   end
   
   str = sprintf('<path d="M %.5f %.5f ',...
       strokes(i).points(1).x, ...
       strokes(i).points(1).y);   
   fwrite(fid, str);
   
   for j=2:length(strokes(i).points)
      str = sprintf('L %.5f %.5f ',strokes(i).points(j).x, strokes(i).points(j).y);   
      fwrite(fid, str);
   end
   
   str = sprintf('"');   
   fwrite(fid, str);
   str = sprintf(' fill="none" stroke="rgb(%d, %d, %d)" stroke-width="%.5f" stroke-opacity="%.3f" number="%d" />',...
                colors(i,1),...
                colors(i,2),...
                colors(i,3),...
                pen_width*avg_pressure,...
                avg_pressure,...
                lines_str_number(i));   
%    str = sprintf(' fill="none" stroke="rgb(%d, %d, %d)" stroke-width="%.5f" stroke-opacity="%.3f" number="%d" />',...
%                 colors(i,1),...
%                 colors(i,2),...
%                 colors(i,3),...
%                 pen_width*avg_pressure,...
%                 1.0,...
%                 lines_str_number(i));   
   fwrite(fid, str);
end

str = sprintf('</svg>');
fwrite(fid, str);

fclose(fid);
end