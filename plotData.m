function plotData(statistics, indConstruction, indDescriptive, obj_dir_path, grp1_spec, grp2_spec)

     h = figure; 
     set(h, 'Name', obj_dir_path);
     k = 3; m = 4;

     i = 1;
     subplot(k,m,i);
     plot(statistics.brightness(indConstruction), statistics.times(indConstruction), grp1_spec);
     hold on;
     plot(statistics.brightness(indDescriptive), statistics.times(indDescriptive), grp2_spec);
     xlabel('brightness');
     ylabel('times');
     legend('construction', 'descriptive');
     
     
     i = i+1;
     subplot(k,m,i);
     plot(statistics.brightness(indConstruction), statistics.lengths(indConstruction), grp1_spec);
     hold on;
     plot(statistics.brightness(indDescriptive), statistics.lengths(indDescriptive), grp2_spec);
     xlabel('brightness');
     ylabel('lengths');
     legend('construction', 'descriptive');

     i = i+1;
     subplot(k,m,i);
     plot(statistics.brightness(indConstruction), statistics.curvatures_v3(indConstruction), grp1_spec);
     hold on;
     plot(statistics.brightness(indDescriptive), statistics.curvatures_v3(indDescriptive), grp2_spec);
     xlabel('brightness');
     ylabel('curvatures');
     legend('construction', 'descriptive');
     
     i = i+1;
     subplot(k,m,i);
     plot(statistics.brightness(indConstruction), statistics.speeds(indConstruction), grp1_spec);
     hold on;
     plot(statistics.brightness(indDescriptive), statistics.speeds(indDescriptive), grp2_spec);
     xlabel('brightness');
     ylabel('speeds');
     legend('construction', 'descriptive');

     i = i+1;
     subplot(k,m,i);
     plot(statistics.lengths(indConstruction), statistics.times(indConstruction), grp1_spec);
     hold on;
     plot(statistics.lengths(indDescriptive), statistics.times(indDescriptive), grp2_spec);
     xlabel('lengths');
     ylabel('times');
     legend('construction', 'descriptive');
     
     i = i+1;
     subplot(k,m,i);
     plot(statistics.lengths(indConstruction), statistics.curvatures_v3(indConstruction), grp1_spec);
     hold on;
     plot(statistics.lengths(indDescriptive), statistics.curvatures_v3(indDescriptive), grp2_spec);
     xlabel('lengths');
     ylabel('curvatures');
     legend('construction', 'descriptive');
     
          i = i+1;
     subplot(k,m,i);
     plot(statistics.lengths(indConstruction), statistics.speeds(indConstruction), grp1_spec);
     hold on;
     plot(statistics.lengths(indDescriptive), statistics.speeds(indDescriptive), grp2_spec);
     xlabel('lengths');
     ylabel('speeds');
     legend('construction', 'descriptive');
          
     i = i+1;
     subplot(k,m,i);
     plot(statistics.curvatures_v3(indConstruction), statistics.times(indConstruction), grp1_spec);
     hold on;
     plot(statistics.curvatures_v3(indDescriptive), statistics.times(indDescriptive), grp2_spec);
     xlabel('curvatures');
     ylabel('times');
     legend('construction', 'descriptive');
     
     i = i+1;
     subplot(k,m,i);
     plot(statistics.times(indConstruction), statistics.speeds(indConstruction), grp1_spec);
     hold on;
     plot(statistics.times(indDescriptive), statistics.speeds(indDescriptive), grp2_spec);
     xlabel('times');
     ylabel('speeds');
     legend('construction', 'descriptive');
     
     i = i+1;
     subplot(k,m,i);
     plot(statistics.curvatures_v3(indConstruction), statistics.speeds(indConstruction), grp1_spec);
     hold on;
     plot(statistics.curvatures_v3(indDescriptive), statistics.speeds(indDescriptive), grp2_spec);
     xlabel('curvatures');
     ylabel('speeds');
     legend('construction', 'descriptive');
     

end