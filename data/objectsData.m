% objects = 
% 
%   1�12 struct array with fields:
% 
%     name
%     num_students_sketched
%     percentage_students_used_line
%     num_students_used_line --- 1 x (number of lines)
function objects = objectsData()
    objects(1).name = 'bumps';
    objects(2).name = 'flange';
    objects(3).name = 'hairdryer';
    objects(4).name = 'house';
    objects(5).name = 'mixer';
    objects(6).name = 'mouse';
    objects(7).name = 'potato_chip';
    objects(8).name = 'shampoo_bottle';
    objects(9).name = 'tubes';
    objects(10).name = 'vacuum_cleaner';
    objects(11).name = 'waffle_iron';
    objects(12).name = 'wobble_surface';
    
    
    objects(1).title = 'bumps';
    objects(2).title = 'flange';
    objects(3).title = 'hairdryer';
    objects(4).title = 'house';
    objects(5).title = 'mixer';
    objects(6).title = 'mouse';
    objects(7).title = 'potato chip';
    objects(8).title = 'shampoo bottle';
    objects(9).title = 'tubes';
    objects(10).title = 'vacuum cleaner';
    objects(11).title = 'waffle iron';
    objects(12).title = 'wobble surface';
    
    objects(1).avg_score = 1.86;
    objects(2).avg_score = 2.29;
    objects(3).avg_score = 3.63;
    objects(4).avg_score = 4.10;
    objects(5).avg_score = 2.00;
    objects(6).avg_score = 3.38;
    objects(7).avg_score = 3.00;
    objects(8).avg_score = 4.00;
    objects(9).avg_score = 2.86;
    objects(10).avg_score = 3.50;
    objects(11).avg_score = 3.86;
    objects(12).avg_score = 2.83;
    
    
    % Indices of the segments that are present in sketches of all designers
    % fot an object.
    objects(1).ref_segment = [9 17];
    objects(2).ref_segment = [13 18];
    objects(3).ref_segment = [5 8];
    objects(4).ref_segment = [3 14];
    objects(5).ref_segment = [10 11];
    objects(6).ref_segment = [9 10];
    objects(7).ref_segment = [1 2];
    objects(8).ref_segment = [10 11];
    objects(9).ref_segment = [12 14];
    objects(10).ref_segment = [10 11];
    objects(11).ref_segment = [9 12];
    objects(12).ref_segment = [1 4];
    
    
    
    ind_scores = [1, 5, 2, 12, 9, 7, 6, 10, 3, 11, 8, 4];
end