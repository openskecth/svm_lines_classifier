
% students = 
% 
%   1�9 struct array with fields:
% 
%     path          --- student identifier
%     experience    --- years of studies
%     objects       --- numbers of objects they draw in objects array
%     object        --- 1�(number of objects) struct array with fields:
%                       .strokesType
%                       .line_present   --- 1 x (number of lines) mask if the
%                                           type of line is present
%                       .statistics     --- 
function [folder_home, students, num_students] = studentsData()
    folder_home = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES_PUBLISH\sketches_json_first_viewpoint';    
%     folder_home_rel = '.\students_clean';
    for i = 1:9
        students(i).folder_home = folder_home;
%         students(i).folder_home_rel = folder_home_rel;
    end
    
    students(1).name  = 'Hamtaro99';
    students(2).name  = 'ilikecoffee';
    students(3).name  = '321testYAY';
    students(4).name  = 'apsnoodijk';
    students(5).name  = 'eentweethee';
    students(6).name  = 'hansworst';
    students(7).name  = 'koffie';
    students(8).name  = 'Patrick33';
    students(9).name  = 'stampertje';
%     
%     students(4).rename = 'student1';
%     students(6).rename = 'student2';
%     students(1).rename = 'student3';
%     students(2).rename = 'student4';
%     students(3).rename = 'student5';
%     students(5).rename = 'student6';
%     students(7).rename = 'student7';
%     students(8).rename = 'student8';
%     students(9).rename = 'student9';
%     
    students(1).id = 'student1';
    students(2).id = 'student2';
    students(3).id = 'student3';
    students(4).id = 'student4';
    students(5).id = 'student5';
    students(6).id = 'student6';
    students(7).id = 'student7';
    students(8).id = 'student8';
    students(9).id = 'student9';

    for i = 1:9
        students(i).designer_type = 'students';
    end
    
    num_students = length(students);
    
%     1 - %'<1'; 2 - '2-3'; 3 - '>3'
    students(1).experience = 1;
    students(2).experience = 1;
    students(3).experience = 2;
    students(4).experience = 3;
    students(5).experience = 3;
    students(6).experience = 3;
    students(7).experience = 3;
    students(8).experience = 3;
    students(9).experience = 3;



    % students(1).objects = {{house},{mixer},{waffle_iron},{wobble_surface}}; 
    % students(2).objects = {{house},{flange},{potato_chip},{wobble_surface}}; 
    % students(3).objects = {{house},{flange},{potato_chip},{wobble_surface}}; 
    % students(4).objects = {{house},{shampoo_bottle},{vacuum_cleaner},{wobble_surface}}; 
    % students(5).objects = {{house},{mouse},{hairdryer},{wobble_surface}}; 
    % students(6).objects = {{house},{bumps},{tubes},{wobble_surface}}; 
    % students(7).objects = {{vacuum_cleaner},{shampoo_bottle},{wobble_surface}}; 
    % students(8).objects = {{house},{mouse},{hairdryer},{wobble_surface}}; 
    % students(9).objects = {{house},{bumps},{tubes},{wobble_surface}}; 

    students(1).objects = [4,8,10,12];
    students(2).objects = [4,1,9,12];
    students(3).objects = [4,5,11,12];
    students(4).objects = [4,2,7,12];
    students(5).objects = [4,2,7,12];
    students(6).objects = [4,6,3,12];
    students(7).objects = [10,8,12];
    students(8).objects = [4,6,3,12];
    students(9).objects = [4,1,9,12];
    
    students(1).objects_v2 = [4,8,10,12];
    students(2).objects_v2 = [4,1,9,12];
    students(3).objects_v2 = [4,5,11,12];
    students(4).objects_v2 = [4,2,7,12];
    students(5).objects_v2 = [4,2,7,12];
    students(6).objects_v2 = [6,3,12];
    students(7).objects_v2 = [10,12];
    students(8).objects_v2 = [4,6,3,12];
    students(9).objects_v2 = [4,1,9,12]; 
    
    students(1).objects_complexity = [NaN,NaN,NaN,NaN];
    students(2).objects_complexity = [4,2,3,4];
    students(3).objects_complexity = [4,1,3,2];
    students(4).objects_complexity = [NaN,NaN,NaN,NaN];
    students(5).objects_complexity = [4,1,3,1];
    students(6).objects_complexity = [NaN,4,4,3];
    students(7).objects_complexity = [NaN,4,3];
    students(8).objects_complexity = [4,2,3,3];
    students(9).objects_complexity = [NaN,NaN,NaN,NaN];
    
    students(1).objects_animation_useful = [NaN,NaN,NaN,NaN];
    students(2).objects_animation_useful = [3,1,1,1];
    students(3).objects_animation_useful = [2,5,3,4];
    students(4).objects_animation_useful = [NaN,NaN,NaN,NaN];
    students(5).objects_animation_useful = [5,2,4,5];
    students(6).objects_animation_useful = [NaN,4,3,4];
    students(7).objects_animation_useful = [NaN,3,4];
    students(8).objects_animation_useful = [5,5,4,4];
    students(9).objects_animation_useful = [NaN,NaN,NaN,NaN];
        
    students(1).drawing_interface_easy = NaN;
    students(2).drawing_interface_easy = 4;
    students(3).drawing_interface_easy = 2;
    students(4).drawing_interface_easy = NaN;
    students(5).drawing_interface_easy = 3;
    students(6).drawing_interface_easy = 3;
    students(7).drawing_interface_easy = 5;
    students(8).drawing_interface_easy = 4;
    students(9).drawing_interface_easy = NaN;

    
    students(1).similar_task = '';
    students(2).similar_task = 'Once per week';
    students(3).similar_task = 'Once per week';
    students(4).similar_task = '';
    students(5).similar_task = 'Once per month';
    students(6).similar_task = 'Once per week';
    students(7).similar_task = 'Once per week';
    students(8).similar_task = 'Every day';
    students(9).similar_task = '';
    
    
    students(1).approximates_from_imagination = NaN;
    students(2).approximates_from_imagination = 3;
    students(3).approximates_from_imagination = 1;
    students(4).approximates_from_imagination = NaN;
    students(5).approximates_from_imagination = 2;
    students(6).approximates_from_imagination = 3;
    students(7).approximates_from_imagination = 3;
    students(8).approximates_from_imagination = 4;
    students(9).approximates_from_imagination = NaN;
    
    
    students(1).tablet_experience = '';
    students(2).tablet_experience = '2-5';
    students(3).tablet_experience = '<1';
    students(4).tablet_experience = '2-5';
    students(5).tablet_experience = '<1';
    students(6).tablet_experience = '2-5';
    students(7).tablet_experience = '<1';
    students(8).tablet_experience = 'p 1';
    students(9).tablet_experience = '<1';
    
    tudents(1).drawign_from_observation = NaN;
    students(2).drawign_from_observation = 3;
    students(3).drawign_from_observation = 5;
    students(4).drawign_from_observation = 4;
    students(5).drawign_from_observation = 2;
    students(6).drawign_from_observation = 5;
    students(7).drawign_from_observation = 4;
    students(8).drawign_from_observation = 2;
    students(9).drawign_from_observation = 3;

    students(1).drawign_from_imagination = NaN;
    students(2).drawign_from_imagination = 3;
    students(3).drawign_from_imagination = 3;
    students(4).drawign_from_imagination = 4;
    students(5).drawign_from_imagination = 3;
    students(6).drawign_from_imagination = 5;
    students(7).drawign_from_imagination = 4;
    students(8).drawign_from_imagination = 4;
    students(9).drawign_from_imagination = 4;
    
    students(1).tools = '';
    students(2).tools = 'Pencils';
    students(3).tools = 'Pencils';
    students(4).tools = 'pencils or ballpen rendered with digital tools';
    students(5).tools = 'Markers';
    students(6).tools = 'Pencils';
    students(7).tools = 'Markers';
    students(8).tools = 'Markers';
    students(9).tools = 'Markers';
    
    students(1).comment = '';
    students(2).comment = '';
    students(3).comment = '';
    students(4).comment = '';
    students(5).comment = '';
    students(6).comment = 'It was difficult that the interface did not allow the sheet to be rotated. This made it had to manage long vertical lines compared to i.e. photoshop where the canvas can be rotated to an angle that is comfortable. Furthermore, the absence of marker or toner tools made it harder to really distinguish the presentation drawing from the initial sketch.';
    students(7).comment = '';
    students(8).comment = '';
    students(9).comment = '';

%     students(1).objects_complexity       = [4 1 3 2];
%     students(1).objects_animation_useful = [2 5 3 4];
%     students(1).drawing_interface_easy = 2;
%     students(1).similar_task = "Once per week";
%     students(1).approximates_from_imagination = 1;
%     students(1).tablet_experience = "<1";
%     students(1).drawign_from_observation = 5;
%     students(1).drawign_from_imagination = 3;
%     students(1).tools = "Pencils";
%     students(1).comment = "";
%     
%     students(2).objects_complexity       = [nan nan nan nan];
%     students(2).objects_animation_useful = [nan nan nan nan];
%     students(2).drawing_interface_easy = nan;
%     students(2).similar_task = "";
%     students(2).approximates_from_imagination = nan;
%     students(2).tablet_experience = "2-5";
%     students(2).drawign_from_observation = 4;
%     students(2).drawign_from_imagination = 4;
%     students(2).tools = "pencils or ballpen rendered with digital tools";
%     students(2).comment = "";
%     
%     students(3).objects_complexity       = [4 1 3 1];
%     students(3).objects_animation_useful = [5 2 4 5];
%     students(3).drawing_interface_easy = 3;
%     students(3).similar_task = "Once per month";
%     students(3).approximates_from_imagination = 2;
%     students(3).tablet_experience = "<1";
%     students(3).drawign_from_observation = 2;
%     students(3).drawign_from_imagination = 3;
%     students(3).tools = "Markers";
%     students(3).comment = "";
%     
%     students(4).objects_complexity       = [nan nan nan nan];
%     students(4).objects_animation_useful = [nan nan nan nan];
%     students(4).drawing_interface_easy = nan;
%     students(4).similar_task = "";
%     students(4).approximates_from_imagination = nan;
%     students(4).tablet_experience = "";
%     students(4).drawign_from_observation = nan;
%     students(4).drawign_from_imagination = nan;
%     students(4).tools = "";
%     students(4).comment = "";
%     
%     students(5).objects_complexity       = [nan 4 4 3 ];
%     students(5).objects_animation_useful = [nan 4 3 4 ];
%     students(5).drawing_interface_easy = 3;
%     students(5).similar_task = "Once per week";
%     students(5).approximates_from_imagination = 3;
%     students(5).tablet_experience = "2-5";
%     students(5).drawign_from_observation = 5;
%     students(5).drawign_from_imagination = 5;
%     students(5).tools = "Pencils";
%     students(5).comment = "It was difficult that the interface did not allow the sheet to be rotated. This made it had to manage long vertical lines compared to i.e. photoshop where the canvas can be rotated to an angle that is comfortable. Furthermore, the absence of marker or toner tools made it harder to really distinguish the presentation drawing from the initial sketch.";
%     
%     % [4,1,9,12]; 
% %     I don't really see the point of doing a second version (presentation
% %     version). I think removing lines usually decreases the information of the drawing; thus making it less communicative;
%     students(6).objects_complexity       = [4 2 3 4];
%     students(6).objects_animation_useful = [3 1 1 1];
%     students(6).drawing_interface_easy = 4;
%     students(6).similar_task = "Once per week";
%     students(6).approximates_from_imagination = 3;
%     students(6).tablet_experience = "2-5";
%     students(6).drawign_from_observation = 3;
%     students(6).drawign_from_imagination = 3;
%     students(6).tools = "Pencils";
%     students(6).comment = "";
%     
%     [10,8,12]; 
%     students(7).objects_complexity       = [nan 4 3];
%     students(7).objects_animation_useful = [nan 3 4];
%     students(7).drawign_from_observation = 4;
%     students(7).drawign_from_imagination = 4;
%     students(7).tools = "Markers";
%     students(7).comment = "";
%     students(7).tablet_experience = "<1";
%     students(7).drawing_interface_easy = 5;
%     students(7).similar_task = "Once per week";
%     students(7).approximates_from_imagination = 3;
%     
%     
%     
%     
% %     [4,6,3,12]; 
%     students(8).objects_complexity       = [4 2 3 3];
%     students(8).objects_animation_useful = [5 5 4 4];
%     
%     students(8).tools = "Markers";
%     students(8).tablet_experience = "p 1";
%     students(8).drawign_from_observation = 2;
%     students(8).drawign_from_imagination = 4;
%     students(8).comment = "";
%     students(8).drawing_interface_easy = 4;
%     students(8).similar_task = "Every day";
%     students(8).approximates_from_imagination = 4;
%     
%     
% %     [4,1,9,12]; 
%     students(9).objects_complexity       = [nan nan nan nan];
%     students(9).objects_animation_useful = [nan nan nan nan];
%     
%     students(9).tools = "Markers";
%     students(9).tablet_experience = "<1";
%     students(9).drawign_from_observation = 3;
%     students(9).drawign_from_imagination = 4;
%     students(9).comment = "";
%     students(9).drawing_interface_easy = nan;
%     students(9).similar_task = "";
%     students(9).approximates_from_imagination = nan;
    

    
    
    
    
    

end