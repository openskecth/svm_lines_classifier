function [ sketch ] = read_sketch( filepath )
   fid = fopen(filepath);
   raw = fread(fid,inf);
   str = char(raw');
   fclose(fid);
   sketch = jsondecode(str);
   sketch = keepOnlyNonRemovedStrokes(sketch);
   fprintf('read file: %s\n', filepath);
end


