function [  labels_predicted, ...
            wrong_labeled_percent, ...
            wrong_labeled_construction, ...
            wrong_labeled_descriptive] = ...
            visualizeTrainigResults(svmmod, designer, features_strs, folder)
objects = objectsData();
[label, ~, ~, ~] = lineTypesData();


wrong_labeled_percent = zeros(length(designer.objects),1);
wrong_labeled_construction = zeros(length(designer.objects),1);
wrong_labeled_descriptive = zeros(length(designer.objects),1);
labels_predicted = cell(length(designer.objects),1);

for j = 1:length(designer.objects)
        obj_num = designer.objects(j);
        
        [obj_dir_path, filepath, filepath_img, filepath_line_types] = ...
                getPaths(designer, objects, obj_num);
     
        
        [statistics, ind]  = getSketchStatistics(filepath, filepath_img);
    
        %Groundtruth labelling:
        strokesType = readJsonFile(filepath_line_types);
        strokesType = strokesType.strokes_line_types;
        [labels_grth, indConsider] = markGrth(strokesType(ind), label);
        
        %Statistics only for those that are relevant:
        statistics.brightness = statistics.brightness(indConsider)';
        statistics.times = statistics.times(indConsider)';
        statistics.lengths = statistics.lengths(indConsider)';
        statistics.speeds = statistics.speeds(indConsider)';
        statistics.curvatures_v3 = statistics.curvatures_v3(indConsider)';
        statistics.stroke_numbers = statistics.stroke_numbers(indConsider)';
        
         X = fillFeatireVector(statistics, features_strs);
        [labels, scores] = predict(svmmod,X);
        labels_predicted{j} = labels;
%         h_samples = plotSamples(X, labels, labels_grth);
        
        img = imread(filepath_img);
        close all;
        figure(2);
        imshow(img);
        
        colors = ['r', 'g'];
        
        close all;
        
        filepath_save = fullfile(folder, [designer.id '_' objects(obj_num).name], 'prediction_svgs');
        if ~exist(filepath_save, 'dir')
            mkdir(filepath_save);
        end
        plotLines(filepath, filepath_img, ind, indConsider, labels, colors, 1, filepath_save);
        
        filename_labels_json = fullfile(folder, 'labels_json', [designer.id '_' objects(obj_num).name '_' 'labels_prediction.json']);
        if ~exist(fullfile(folder, 'labels_json'), 'dir')
            mkdir(fullfile(folder, 'labels_json'));
        end
        saveLabelingAsJSONFile(filename_labels_json, labels);
        
        
        
        
        h1 = figure(1);        
%         print(fullfile(folder, [designer.id '_' objects(obj_num).name '_construction.pdf']), '-dpdf');
        print(fullfile(folder, [designer.id '_' objects(obj_num).name '_construction.png']), '-dpng');
        h2 = figure(2);        
        print(fullfile(folder, [designer.id '_' objects(obj_num).name '_descriptive.png']), '-dpng');
        
        
        
        colors = ['m', 'c'];
%         h_grth = figure(4);
        filepath_save = fullfile(folder, [designer.id '_' objects(obj_num).name], 'grth_svgs');
        if ~exist(filepath_save, 'dir')
            mkdir(filepath_save);
        end
        plotLines(filepath, filepath_img, ind, indConsider, labels_grth, colors,3, filepath_save);
        
                
        filename_labels_json = fullfile(folder, 'labels_json', [designer.id '_' objects(obj_num).name '_' 'labels_grth.json']);
        if ~exist(fullfile(folder, 'labels_json'), 'dir')
            mkdir(fullfile(folder, 'labels_json'));
        end
        saveLabelingAsJSONFile(filename_labels_json, labels_grth);
        
        h1 = figure(3);        
%         print(fullfile(folder, [designer.id '_' objects(obj_num).name '_construction_grth.pdf']), '-dpdf');
        print(fullfile(folder, [designer.id '_' objects(obj_num).name '_construction_grth.png']), '-dpng');
        h2 = figure(4);        
        print(fullfile(folder, [designer.id '_' objects(obj_num).name '_descriptive_grth.png']), '-dpng');
        
%         print(fullfile(folder, [designer.id '_' objects(obj_num).name '.pdf']), '-dpdf');
        
        num_samples = length(labels);
        wrong_labeled_percent(j) = sum(labels~=labels_grth)/num_samples;
        wrong_labeled_construction(j)  = sum(labels==-1)/sum(labels_grth==-1);
        wrong_labeled_descriptive(j)   = sum(labels==1)/sum(labels_grth==1);
end

end

function saveFigures(path_save, h_samples, h_estimated, h_grth)
    saveas(h_samples, [path_save '_samples.fig']);
    saveas(h_estimated, [path_save '_estimated.fig']);
    saveas(h_grth, [path_save '_grth.fig']);
end

function h = plotSamples(X, labels, labels_grth)
        h = figure(1);
        m = 4;
        k = 2;
%         subplot(m,k,1);
%         gscatter(X(:,1),X(:,2),labels,'rg','+*');
%         xlabel('times');
%         ylabel('lengths');
%         legend('Construction','Descriptive');
%         title('Predicted');
        
%         subplot(m,k,2);
%         gscatter(X(:,1),X(:,2),labels_grth,'mc','+*');
%         xlabel('times');
%         ylabel('lengths');
%         legend('Construction','Descriptive');
%         title('Grth');
%       
        i = 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,2),labels,'rg','+*');
        xlabel('times');
        ylabel('speeds');
        legend('Construction','Descriptive');
        title('Predicted');
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,2),labels_grth,'mc','+*');
        xlabel('times');
        ylabel('speeds');
        legend('Construction','Descriptive');
        title('Grth');
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,3),labels,'rg','+*');
        xlabel('times');
        ylabel('brightness');
        legend('Construction','Descriptive');
        title('Predicted');
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,3),labels_grth,'mc','+*');
        xlabel('times');
        ylabel('brightness');
        legend('Construction','Descriptive');
        title('Grth');
        
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,4),labels,'rg','+*');
        xlabel('times');
        ylabel('lengths');
        legend('Construction','Descriptive');
        title('Predicted');
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,4),labels_grth,'mc','+*');
        xlabel('times');
        ylabel('lengths');
        legend('Construction','Descriptive');
        title('Grth');
        
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,5),labels,'rg','+*');
        xlabel('times');
        ylabel('curvature');
        legend('Construction','Descriptive');
        title('Predicted');
        
        i = i + 1;
        subplot(m,k,i);
        gscatter(X(:,1),X(:,5),labels_grth,'mc','+*');
        xlabel('times');
        ylabel('curvature');
        legend('Construction','Descriptive');
        title('Grth');
end



function plotLines(filepath, filepath_img, ind, indConsider, labels, colors, i, filepath_save)

    indConstruction = labels == -1;
    indDescriptive = labels == 1;
    
    img = imread(filepath_img);
     
%     subplot(1,3,1);
%     imshow(img);
    
  
    sketch = read_sketch(filepath);
    strokes = sketch.strokes;
    
    strokes = strokes(ind);
    strokes = strokes(indConsider);
    
    width = size(img,1);
    
    figure(i);
    imshow(img);
    hold on;
    strokes_constrcution = strokes(indConstruction);
    displayStrokes(strokes_constrcution, colors(1), width);    
    axis equal
    axis off;
    
    figure(i+1);
    imshow(img);
    hold on;
    strokes_descriptive = strokes(indDescriptive);
    displayStrokes(strokes_descriptive, colors(2), width);
    axis equal
    axis off;
    
    
    save_as_svg_line_type(strokes_constrcution, width, fullfile(filepath_save, 'construction.svg'), find(labels == -1));
    save_as_svg_line_type(strokes_descriptive, width, fullfile(filepath_save, 'descriptive.svg'), find(labels == 1));
end

function displayStrokes(strokes, color, width)
  for str_num = 1:length(strokes)
        stroke_points = strokes(str_num).points;
        x = [];
        y = [];
        j = 0;
        for i = 1:length(stroke_points)
            
            x_= stroke_points(i).x;
            y_= stroke_points(i).y;
            if ((x_ > 0) && (y_ > 0) && (x_ <= width) && (y_ <= width))
                j = j+1;
                x(j) = stroke_points(i).x;
                y(j) = stroke_points(i).y;
            end
        end
        plot(x,y,color);
  end
end

function [obj_dir_path, filepath, filepath_img, filepath_line_types, path_save] = ...
                getPaths(designer, objects, obj_num)
    view = 'view1';
    obj_dir_path = fullfile(designer.folder_home, designer.id,...
        objects(obj_num).name);
    
    filepath        = fullfile(obj_dir_path, [view '_concept.json']);  
    filepath_img    = fullfile(obj_dir_path, [view '_concept_opaque.png']);
    
      folder_ = fileparts(designer.folder_home);   
     
     filepath_line_types = fullfile( folder_, 'sketches_labeling_first_viewpoint', designer.id,...
                                     objects(obj_num).name, 'strokes_lines_types_view1_concept.json');   
                                 
    
        
    folder_save = fullfile('./results/', designer.id);
    path_save = fullfile(folder_save, objects(obj_num).name);
    if (~exist(folder_save, 'dir'))
        mkdir(folder_save);
    end
end