function [labels,indConsider] = markGrth(strokesType, label)
    constructionSet = [label.context_axis_and_grids:label.hinging_and_rotating_elements label.ridges_occluded label.valleys_occluded];
    descriptiveSet = [label.occluding_contour:label.vallleys_visible label.cross_sections];
    
    indConstruction = ismember(strokesType, constructionSet);
    indDescriptive = ismember(strokesType, descriptiveSet);
    indConsider = indConstruction  | indDescriptive;
    
    num_strokes = length(strokesType);
    labels = ones(num_strokes, 1);
    labels(indConstruction) = -1;
    labels = labels(indConsider);
end
