function [s, indicesSamples] = selectSubsetOfFeatures(s, num_samples)
% Input:    
%   s: structure that contains statistics of each stroke as well as it
%   label.
%     s.brightness
%     s.times     
%     s.lengths   
%     s.speeds    
%     s.curvatures_v3  
%     s.labels     
%     s.indConstruction  
%     s.indDescriptive  
%     
%     num_samples: 
%       number of samples to extract in each of two groups

    num_samples = min([num_samples, sum(s.indConstruction), sum(s.indDescriptive)]);
    
    s.indConstruction   = sort(find(s.labels == -1));
    s.indDescriptive    = sort(find(s.labels ==  1));

    s.indConstruction = datasample(s.indConstruction,num_samples, 'Replace',false);
    s.indDescriptive = datasample(s.indDescriptive,num_samples, 'Replace',false);
     
    indicesSamples = union(s.indConstruction, s.indDescriptive);
    
    s.brightness        = s.brightness(indicesSamples);
    s.times             = s.times(indicesSamples);
    s.lengths           = s.lengths(indicesSamples);
    s.speeds            = s.speeds(indicesSamples);
    s.curvatures_v3     = s.curvatures_v3(indicesSamples);
    s.labels            = s.labels(indicesSamples);   
    s.stroke_numbers    = s.stroke_numbers(indicesSamples);   
    s.indConstruction = s.labels == -1;
    s.indDescriptive = s.labels == 1;
end