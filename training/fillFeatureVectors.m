function [s, obj_dir_path] = fillFeatureVectors(designers, designers_train_indices, ...
                                objects, objects_numbers_train_designers)
[label, ~, ~, ~] = lineTypesData();

view = 'view1';

s.brightness        = [];
% brightness        = cell(length(objects_numbers_train),1);
s.times             = [];
s.stroke_numbers    = [];
s.lengths           = [];
% lengths           = cell(length(objects_numbers_train),1);
s.speeds            = [];
% speeds            = cell(length(objects_numbers_train),1);
s.curvatures_v3     = [];

s.labels            = [];
s.indConstruction   = [];
% indConstruction   = cell(length(objects_numbers_train),1);
s.indDescriptive    = [];
% indDescriptive    = cell(length(objects_numbers_train),1);

ind_sketch =0;




for i = designers_train_indices 
    designer = designers(i);
    objects_numbers_train = objects_numbers_train_designers{i};
    i
 for j = 1:length(objects_numbers_train)
     j
     ind_sketch = ind_sketch + 1
     obj_num = objects_numbers_train(j);
     obj_dir_path = fullfile(designer.folder_home, designer.id,...
        objects(obj_num).name);
     filepath           = fullfile(obj_dir_path, [view '_concept.json']);  
     filepath_img       = fullfile(obj_dir_path, [view '_concept_opaque.png']);  
     [statistics, ind]  = getSketchStatistics(filepath, filepath_img);
     
     folder_ = fileparts(designer.folder_home);   
     
     filepath_line_types = fullfile( folder_, 'sketches_labeling_first_viewpoint', designer.id,...
                                     objects(obj_num).name, 'strokes_lines_types_view1_concept.json');       
     strokesType = readJsonFile(filepath_line_types);
     strokesType = strokesType.strokes_line_types;
     
     [labels, indConsider] = markGrth(strokesType(ind), label);
     
     
     statistics.brightness = statistics.brightness(indConsider);
     statistics.times = statistics.times(indConsider);
     statistics.lengths = statistics.lengths(indConsider);
     statistics.speeds = statistics.speeds(indConsider);
     statistics.curvatures_v3 = statistics.curvatures_v3(indConsider);
     statistics.stroke_numbers = statistics.stroke_numbers(indConsider);
    
%      [  mean_vals_brightness(j, :), ...
%         mean_vals_times(j, :), ...    
%         mean_vals_stroke_numbers(j, :), ...
%         mean_vals_lengths(j, :), ...
%         mean_vals_curvatures(j, :), ...
%         mean_vals_speeds(j, :)...
%         ] = visualizeStatisticsGroups(statistics, indConstruction, indDescriptive);
%      
     grp1_spec = 'ro';
     grp2_spec = 'go';
     
%      plotData(statistics, indConstruction, indDescriptive, obj_dir_path, grp1_spec, grp2_spec);
%      s.brightness{j}   = [s.brightness;  statistics.brightness'];
%      s.times{j}        = [s.times;  statistics.times'];
%      s.speeds{j}       = [s.speeds;  statistics.speeds'];
%      s.lengths{j}      = [s.lengths;  statistics.lengths'];
%      s.curvatures_v3{j}= [s.curvatures_v3;  statistics.curvatures_v3'];

     brightness{ind_sketch}      = statistics.brightness';
     s.times            = [s.times;  statistics.times'];
     s.stroke_numbers   = [s.stroke_numbers;  statistics.stroke_numbers'];
     speeds{ind_sketch}          = statistics.speeds';
     lengths{ind_sketch}         = statistics.lengths';
     s.curvatures_v3    = [s.curvatures_v3;  statistics.curvatures_v3'];
     
     indConstruction_ = labels == -1;
     indDescriptive_ = labels == 1;
     
     indConstruction{ind_sketch} = indConstruction_;
     indDescriptive{ind_sketch} = indDescriptive_;
     
     s.labels = [s.labels; labels];
     grp1_spec = 'co';
     grp2_spec = 'mo';
     
%      plotData(s, logical(s.indConstruction'), logical(s.indDescriptive'), obj_dir_path, grp1_spec, grp2_spec);
     
%      Data = [Data; statistics.brightness' statistics.times' statistics.lengths' statistics.curvatures_v3'];
     
 end
end
 for j = 1:ind_sketch
     s.brightness   = [s.brightness; brightness{j}];
     s.speeds       = [s.speeds; speeds{j}];
     s.lengths      = [s.lengths; lengths{j}];
     
 end
 
 
 s.indConstruction = (s.labels == -1);
 s.indDescriptive  = (s.labels == 1);
     
     
% selectNormalisation(s.brightness, s.indConstruction, s.indDescriptive);

%   plotData(s, logical(s.indConstruction'), logical(s.indDescriptive'), obj_dir_path, grp1_spec, grp2_spec);
 
end

