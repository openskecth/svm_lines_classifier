function [statistics, ind] = getSketchStatistics(filepath, filepath_img)
    sketch = read_sketch(filepath);
    img = imread(filepath_img);
    strokes = sketch.strokes;
%     pen_width = sketch.canvas.pen_width;
    num_strokes = length(strokes);
    
    lengths         = zeros(1, num_strokes);
    speeds          = zeros(1, num_strokes);
    brightness      = zeros(1, num_strokes);
    curvatures_v1   = zeros(1, num_strokes);
    curvatures_v2   = zeros(1, num_strokes);
    curvatures_v3   = zeros(1, num_strokes);
    
    times = zeros(1, num_strokes);
    stroke_numbers = zeros(1, num_strokes);
    
    p_num = 0;
%     figure(1);
    for str_num = 1:length(strokes)
        stroke_points = strokes(str_num).points;
        
%         x = zeros(length(stroke_points), 1);
%         y = zeros(length(stroke_points), 1);
        for i = 1:length(stroke_points)
           p_num = p_num+1;
           x(p_num) = stroke_points(i).x;
           y(p_num) = stroke_points(i).y;
        end
        
%         imshow(img);
%         hold on;
%         plot(x,y,'*-');
%         hold off;
% %         
        
        lengths(str_num) = lengthStroke(stroke_points);
        speeds(str_num) = speedStroke(stroke_points, lengths(str_num));
        brightness(str_num) = brightnessStroke(stroke_points);
        times(str_num) = timeCreationStroke(stroke_points);
        curvatures_v1(str_num) = curvatureStrokeV1(stroke_points);
        curvatures_v2(str_num) = curvatureStrokeV2(stroke_points);
        curvatures_v3(str_num) = curvatureStrokeV3(stroke_points);
        stroke_numbers(str_num) = str_num;
        
%         fprintf('-------- \n');
%         fprintf('length = %.3f\n',  lengths(str_num));
%         fprintf('speed = %.3f\n',  speeds(str_num));
%         fprintf('brightness = %.3f\n',  brightness(str_num));
%         fprintf('time = %.3f\n',  times(str_num));
%         fprintf('curvature = %.3f, %.3f, %.3f\n',  curvatureStrokeV1(stroke_points),  curvatureStrokeV2(stroke_points),  curvatureStrokeV3(stroke_points));
 
    end
    
    ind = ~isnan(curvatures_v3) & ~isinf(speeds);
    
    % Normalize brightness values:
%     brightness = brightness/max(brightness(:));
    
%     p_length = prctile(lengths, 99);
%     
%     lengths = lengths/p_length;
%     
%     scale = findScale([x; y]');
%     lengths = lengths*scale;
%     lengths = lengths /max(lengths(:));
    
    statistics.lengths = lengths(ind);
    statistics.speeds = speeds(ind);
    statistics.brightness = brightness(ind);
    statistics.times = times(ind);
    statistics.stroke_numbers = stroke_numbers(ind);
    statistics.curvatures_v1 = curvatures_v1(ind);
    statistics.curvatures_v2 = curvatures_v2(ind);
    statistics.curvatures_v3 = curvatures_v3(ind);
    
    statistics.times = statistics.times/max(statistics.times(:));
    statistics.stroke_numbers  = statistics.stroke_numbers /length(statistics.stroke_numbers );
    
    statistics.lengths = statistics.lengths/prctile(statistics.lengths, 95);
    statistics.brightness = statistics.brightness/prctile(statistics.brightness, 95);
    statistics.speeds = statistics.speeds/prctile(statistics.speeds, 95);
end

function length = lengthStroke(stroke_points)
    length = 0.0;
    
    for p = 1:(size(stroke_points,1)-1)
        dx = stroke_points(p+1).x - stroke_points(p).x;
        dy = stroke_points(p+1).y - stroke_points(p).y;
        length = length + sqrt(dx*dx + dy*dy);
    end

end

function speed = speedStroke(stroke_points, length)
  dt = stroke_points(end).t - stroke_points(1).t;
  speed = length/dt;
end

function brightness = brightnessStroke(stroke_points)
    brightness = 0.0;
    for p = 1:size(stroke_points,1)
        brightness = brightness + stroke_points(p).p;
    end
    brightness = brightness/length(stroke_points);
end

function time = timeCreationStroke(stroke_points)
    time = (stroke_points(end).t + stroke_points(1).t)/2;
end

function res = pointsAreEqual(p1, p2)
    res = p1.x == p2.x || p1.y == p2.y;
end

function curvature = curvatureStrokeV1(stroke_points)
%https://www.intmath.com/applications-differentiation/8-radius-curvature.php
    curvature = 0.0;
    num_vals = 0;
    for p = 2:(size(stroke_points,1)-1)
        if (pointsAreEqual(stroke_points(p-1), stroke_points(p)) || pointsAreEqual(stroke_points(p), stroke_points(p+1)))
            continue;
        end
%         stroke_points(p-1).x, stroke_points(p).x, stroke_points(p+1).x
%         stroke_points(p-1).y, stroke_points(p).y, stroke_points(p+1).y
        
        curvature = curvature + computeLocalCurvatureFitPorabola(stroke_points(p-1), stroke_points(p), stroke_points(p+1));
        num_vals = num_vals + 1;
    end
    curvature = curvature/num_vals;
end



function curvature = curvatureStrokeV2(stroke_points)
%https://www.intmath.com/applications-differentiation/8-radius-curvature.php
    curvature = 0.0;
     num_vals = 0;
    for p = 2:(size(stroke_points,1)-1)
       if (pointsAreEqual(stroke_points(p-1), stroke_points(p)) || pointsAreEqual(stroke_points(p), stroke_points(p+1)))
            continue;
       end
        num_vals = num_vals + 1;
        curvature = curvature + computeLocalCurvatureLinearApproximation(stroke_points(p-1), stroke_points(p), stroke_points(p+1));
    end
    curvature = curvature/num_vals;
end


function curvature = curvatureStrokeV3(stroke_points)
%https://www.intmath.com/applications-differentiation/8-radius-curvature.php
    curvature = 0.0;
     num_vals = 0;
    for p = 2:(size(stroke_points,1)-1)
       if (pointsAreEqual(stroke_points(p-1), stroke_points(p)) || pointsAreEqual(stroke_points(p), stroke_points(p+1)))
            continue;
       end
        num_vals = num_vals + 1;
        curvature = curvature + computeLocalCurvatureFitCircle(stroke_points(p-1), stroke_points(p), stroke_points(p+1));
    end
    curvature = curvature/num_vals;
end

function curvature = computeLocalCurvatureFitPorabola(point1, point2, point3)
    A =  [point1.x^2 point1.x 1; 
          point2.x^2 point2.x 1; 
          point3.x^2 point3.x 1]; 

    b = [point1.y; point2.y; point3.y]; 
    s = A\b;
    
    dydivdx = 2*s(1)*point2.x + s(2);
    d2ydivdx2 = 2*s(1);
    r = (1 + dydivdx^2)^(1.5) / abs(d2ydivdx2);
    curvature = 1/r;
end


function curvature = computeLocalCurvatureLinearApproximation(point1, point2, point3)
    m1 = (point2.y - point1.y)/(point2.x - point1.x);
    m2 = (point3.y - point2.y)/(point3.x - point2.x);
    
    dydivdx = (m1+m2)/2.0;
    d2ydivdx2 = 2*(m2 - m1)/(point3.x - point1.x);
    
    r = (1 + dydivdx^2)^(1.5) / abs(d2ydivdx2);
    curvature = 1/r;
end

function curvature = computeLocalCurvatureFitCircle(point1, point2, point3)
    m1 = (point2.y - point1.y)/(point2.x - point1.x);
    m2 = (point3.y - point2.y)/(point3.x - point2.x);
    
    nom     = m1*m2*(point1.y - point3.y) + m2*(point1.x + point2.x) - m1*(point2.x + point3.x);
    denom   = 2*(m2-m1);
    x_c  = nom/denom;
    y_c = -1/m1*(x_c - (point1.x + point2.x)/2.0) + (point1.y + point2.y)/2.0 ;
    
    r = sqrt((point1.x - x_c)^2+(point1.y - y_c)^2);
    curvature = 1/r;
end




% That is the same as brightness:
% function widthAvgStroke = averageWidthStroke(stroke_points, pen_width)
%     widthAvgStroke = 0.0;
%      for p = 1:length(stroke_points)
%         widthAvgStroke = widthAvgStroke + stroke_points(p).p*pen_width;
%      end
%     widthAvgStroke = widthAvgStroke/length(stroke_points);
% end