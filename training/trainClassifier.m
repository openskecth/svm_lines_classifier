function [svmmod, X, Y] = ...
        trainClassifier(designers, designers_train_indices, objects_numbers_train_designers, features_strs)
    
    objects = objectsData();
            
%     [statistics, obj_dir_path] = ...
%         fillDataOneDesigner(designer, objects, objects_numbers_train);
    
    [statistics, obj_dir_path] = fillFeatureVectors(designers, designers_train_indices, ...
                                    objects, objects_numbers_train_designers);
                            

    num_samples = 1000; %Number of samples for each class
    [statistics, ~] = selectSubsetOfFeatures(statistics, num_samples);
    
    grp1_spec = 'co';
    grp2_spec = 'm*';
     
    plotData(statistics, statistics.indConstruction', statistics.indDescriptive', ...
                obj_dir_path, grp1_spec, grp2_spec); 

    
            
    X = fillFeatireVector(statistics, features_strs);
    Y = [statistics.labels];

    nels = length(Y);
    c = cvpartition(nels,'KFold',10);
    opts = struct('Optimizer','bayesopt','ShowPlots',true,'CVPartition',c,...
        'AcquisitionFunctionName','expected-improvement-plus');
    svmmod = fitcsvm(X,Y,'KernelFunction','rbf',...
        'OptimizeHyperparameters','auto','HyperparameterOptimizationOptions',opts);

% lossnew = kfoldLoss(fitcsvm(X,Y,'CVPartition',c,'KernelFunction','rbf',...
%     'BoxConstraint',svmmod.HyperparameterOptimizationResults.XAtMinObjective.BoxConstraint,...
%     'KernelScale',svmmod.HyperparameterOptimizationResults.XAtMinObjective.KernelScale));

end

% function [designers, objects] = loadData()
%     [~, designers(1, 1:9), ~]   = studentsData();
%     [~, designers(1, 10:15), ~] = professionalsData();
% 
%     objects = objectsData();
% end
% 
