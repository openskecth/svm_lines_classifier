function [ designers_train,designers_test] = trainSubsetDesigners()
warning off;
designers_train = [1 3:4 6:7 8 10:11 13 15];
designers_test = [2 5 9 12 14];

[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();



fileTestTrainObjectNumbers = './testTrainObjectNumbers.mat';

if (~exist(fileTestTrainObjectNumbers, 'file'))
    [objects_numbers_train, objects_numbers_test] = distributeTestAndTrainSets(designers);
    save(fileTestTrainObjectNumbers, 'objects_numbers_train', 'objects_numbers_test');
else
    load(fileTestTrainObjectNumbers, 'objects_numbers_train', 'objects_numbers_test');
end


designer_str ='all_ar3';
% features = {'brightness'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%     objects_numbers_train,features);
% 
% % features = {'times'};
% % trainAndValidateOneDesigner(designer, objects_numbers_train,features);
% 
% features = {'stroke_numbers'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%     objects_numbers_train,features);
% 
% features = {'lengths'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%     objects_numbers_train,features);
% 
% features = {'speeds'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%     objects_numbers_train,features);
% 
% features = {'curvatures'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%      objects_numbers_train,features);
% 
% features = {'brightness',...            
%             'stroke_numbers',...
%             'speeds',...
%             'curvatures'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%      objects_numbers_train,features);
% 
features = {'brightness',...
            'stroke_numbers',...
            'speeds'};
trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
    objects_numbers_train,features);
% 
% features = {'brightness',...
%             'stroke_numbers',...
%             'speeds',...
%             'lengths'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%      objects_numbers_train,features);

% features = {'brightness',...
%             'stroke_numbers',...
%             'speeds',...
%             'lengths',...
%             'curvatures'};
% trainAndValidateOneDesigner(designers, designers_train, designer_str, ...
%      objects_numbers_train,features);

end