The code used to train svm lines classifier.

The code needs changing of naming convention of input files.

## Data
Sketches can be downloaded here:
https://repo-sam.inria.fr/d3/OpenSketch/Data/sketches/sketches_json_first_viewpoint.zip

Line labels can be downloaded here:
https://repo-sam.inria.fr/d3/OpenSketch/Data/labeling/sketches_labeling_first_viewpoint.zip


## Training results
The trained model can be dowloaded here:
https://repo-sam.inria.fr/d3/OpenSketch/Data/Applications/clustering/svm_lines_classification_results.zip


## This code is a part of OpenSketch publication
Project page:
https://ns.inria.fr/d3/OpenSketch/

Additional data:
https://repo-sam.inria.fr/d3/OpenSketch/

## Code usage
To train each designer individually please run "run.m"
To train on a subset of designers run trainSubsetDesigners.m

Specify the correct path to the skecthes ('your_path/sketches_json_first_viewpoint') in studentsData.m and professionalData.m.

## Contact
If you have any questions please contact Yulia Gryaditskaya yulia.gryaditskaya@gmail.com
