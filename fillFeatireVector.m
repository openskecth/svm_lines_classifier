function X = fillFeatireVector(statistics, features_strs)
    X = [];
    if ismember('brightness', features_strs)
        X = [X, statistics.brightness];
    end
    if ismember('times', features_strs)
        X = [X, statistics.times];
    end
    if ismember('stroke_numbers', features_strs)
        X = [X, statistics.stroke_numbers];
    end
    if ismember('lengths', features_strs)
        X = [X, statistics.lengths];
    end
    if ismember('speeds', features_strs)
        X = [X, statistics.speeds];
    end
    if ismember('curvatures', features_strs)
        X = [X, statistics.curvatures_v3];
    end
    
end