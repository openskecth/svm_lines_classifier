function table = makeTableTrainingResultsCompareAll(designers_train, designers_test)

[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

fileTestTrainObjectNumbers = './testTrainObjectNumbers.mat';

if (~exist(fileTestTrainObjectNumbers, 'file'))
    [objects_numbers_train, objects_numbers_test] = distributeTestAndTrainSets(designers);
    save(fileTestTrainObjectNumbers, 'objects_numbers_train', 'objects_numbers_test');
else
    load(fileTestTrainObjectNumbers, 'objects_numbers_train', 'objects_numbers_test');
end
% 
% designers_train = [1 3:4 6:7 8 10:11 14:15];
% designers_test = [2 5 9 12 13];

designer_str ='all_ar2';
folder = fullfile('./results', designer_str);
subfolder = '_brightness_stroke_numbers_speeds_lengths_curvatures';
filename = fullfile(folder, subfolder, 'classifier.mat');

if (exist(filename, 'file'))
    data = load(filename);
    
    %Compute the test and train objects, designers from the training set
    wrong_labeled_percent_train_designer_train_object = [];
    wrong_labeled_percent_train_designer_test_object = [];
    
    for des_num = designers_train
        objects_numbers_train_designer = objects_numbers_train{des_num};
        objects_numbers_test_designer = objects_numbers_test{des_num};
        designer = designers(des_num);
        [~,objects_numbers_train_order] = ismember(objects_numbers_train_designer, designer.objects);
        [~,objects_numbers_test_order] = ismember(objects_numbers_test_designer, designer.objects);
        wrong_labeled_percent_train_designer_train_object = ...
            [wrong_labeled_percent_train_designer_train_object; ...
            data.wrong_labeled_percent{des_num}(objects_numbers_train_order)];
        wrong_labeled_percent_train_designer_test_object = ...
            [wrong_labeled_percent_train_designer_test_object; ...
            data.wrong_labeled_percent{des_num}(objects_numbers_test_order)];
    end
    
    %Compute the test and train objects, designers from the test set
    wrong_labeled_percent_test_designer_train_object = [];
    wrong_labeled_percent_test_designer_test_object = [];
    
    for des_num = designers_test
        objects_numbers_train_designer = objects_numbers_train{des_num};
        objects_numbers_test_designer = objects_numbers_test{des_num};
        designer = designers(des_num);
        [~,objects_numbers_train_order] = ismember(objects_numbers_train_designer, designer.objects);
        [~,objects_numbers_test_order] = ismember(objects_numbers_test_designer, designer.objects);
        wrong_labeled_percent_test_designer_train_object = ...
            [wrong_labeled_percent_test_designer_train_object; ...
            data.wrong_labeled_percent{des_num}(objects_numbers_train_order)];
        wrong_labeled_percent_test_designer_test_object = ...
            [wrong_labeled_percent_test_designer_test_object; ...
            data.wrong_labeled_percent{des_num}(objects_numbers_test_order)];
    end
    
    wrong_labeled_percent_train_designer_train_object   = 1.0 - wrong_labeled_percent_train_designer_train_object;
    wrong_labeled_percent_train_designer_test_object    = 1.0 - wrong_labeled_percent_train_designer_test_object;
    wrong_labeled_percent_test_designer_train_object    = 1.0 - wrong_labeled_percent_test_designer_train_object;
    wrong_labeled_percent_test_designer_test_object     = 1.0 - wrong_labeled_percent_test_designer_test_object;
    
    table{1,1} = mean(wrong_labeled_percent_train_designer_train_object(:))*100;
    table{1,2} = mean(wrong_labeled_percent_train_designer_test_object(:))*100;
    table{1,3} = mean(wrong_labeled_percent_test_designer_train_object(:))*100;
    table{1,4} = mean(wrong_labeled_percent_test_designer_test_object(:))*100;
    
    table{2,1} = median(wrong_labeled_percent_train_designer_train_object(:))*100;
    table{2,2} = median(wrong_labeled_percent_train_designer_test_object(:))*100;
    table{2,3} = median(wrong_labeled_percent_test_designer_train_object(:))*100;
    table{2,4} = median(wrong_labeled_percent_test_designer_test_object(:))*100;
        
    table{3,1} = std(wrong_labeled_percent_train_designer_train_object(:))*100;
    table{3,2} = std(wrong_labeled_percent_train_designer_test_object(:))*100;
    table{3,3} = std(wrong_labeled_percent_test_designer_train_object(:))*100;
    table{3,4} = std(wrong_labeled_percent_test_designer_test_object(:))*100;
    
end



end
