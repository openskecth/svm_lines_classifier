function [objects_numbers_train, objects_numbers_test] = distributeTestAndTrainSets(designers)
    num_designers = length(designers);
    objects_numbers_train = cell(num_designers,1);
    objects_numbers_test = cell(num_designers,1);
    wobble_surface_num = 12;
    for i = 1:9
        designer = designers(i);
        objects_numbers_test{i} = wobble_surface_num;%wobble surface
        objects_numbers_train{i} = setxor(designer.objects, objects_numbers_test{i});
    end
    
    
    num_objects = 12;
    objects = setxor(designer.objects, wobble_surface_num);        
    k = int8(num_objects/4.0)-1;
%         objects_numbers_train{i} = datasample(designer.objects,k, 'Replace',false);
%         objects_numbers_test{i} = setxor(designer.objects, objects_numbers_train{i});
    objects_numbers_test_prof = [datasample(objects, k, 'Replace',false) 12];
    objects_numbers_train_prof = setxor([1:12], objects_numbers_test_prof);
    
    
    for i = 10:num_designers       
        objects_numbers_test{i}       = objects_numbers_test_prof;
        objects_numbers_train{i} = objects_numbers_train_prof;
    end
    
end