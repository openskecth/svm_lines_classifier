function row_f = comparePerformanceFeatures(designers, ind_designers_test, designer_str,...
    objects_numbers_train_designer, objects_numbers_test_designer)

designer  = designers(ind_designers_test);
mean_train_brightness = NaN;
mean_test_brightness = NaN;

mean_train_curvature = NaN;
mean_test_curvature = NaN;

mean_train_lengths = NaN;
mean_test_lengths = NaN;

mean_train_speeds = NaN;
mean_test_speeds = NaN;

mean_train_stroke_numbers = NaN;
mean_test_stroke_numbers = NaN;

mean_train_brightness_stroke_numbers_speeds = NaN;
mean_test_brightness_stroke_numbers_speeds = NaN;

mean_train_brightness_stroke_numbers_speeds_curvatures = NaN;
mean_test_brightness_stroke_numbers_speeds_curvatures = NaN;

mean_train_brightness_stroke_numbers_speeds_lengths = NaN;
mean_test_brightness_stroke_numbers_speeds_lengths = NaN;

mean_train_brightness_stroke_numbers_speeds_lengths_c = NaN;
mean_test_brightness_stroke_numbers_speeds_lengths_c = NaN;

row = cell(1, 18);



folder = fullfile('./results', designer_str);

% subfolder = '_brightness_times_stroke_numbers_lengths_speeds_curvatures';
% filename = fullfile(folder, subfolder, 'classifier.mat');
% data = load(filename);
% wrong_labeled_percent_all = data.wrong_labeled_percent;
% mean_train_all = mean(wrong_labeled_percent_all(objects_numbers_train));
% mean_test_all = mean(wrong_labeled_percent_all(objects_numbers_test));
[~,objects_numbers_train_order] = ismember(objects_numbers_train_designer, designer.objects);
[~,objects_numbers_test_order] = ismember(objects_numbers_test_designer, designer.objects);


subfolder = '_brightness';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_brightness = data.wrong_labeled_percent{ind_designers_test};
    mean_train_brightness = mean(wrong_labeled_percent_brightness(objects_numbers_train_order));
    mean_test_brightness = mean(wrong_labeled_percent_brightness(objects_numbers_test_order));
    fprintf('brightness: \n train = %.3f, test = %.3f\n',...
        mean_train_brightness, mean_test_brightness);
end

subfolder = '_curvatures';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_curvature = data.wrong_labeled_percent{ind_designers_test};
    mean_train_curvature = mean(wrong_labeled_percent_curvature(objects_numbers_train_order));
    mean_test_curvature = mean(wrong_labeled_percent_curvature(objects_numbers_test_order));
    fprintf('curvature: \n train = %.3f, test = %.3f\n',...
        mean_train_curvature, mean_test_curvature);
end


subfolder = '_lengths';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_lengths = data.wrong_labeled_percent{ind_designers_test};
    mean_train_lengths = mean(wrong_labeled_percent_lengths(objects_numbers_train_order));
    mean_test_lengths  = mean(wrong_labeled_percent_lengths(objects_numbers_test_order));

    fprintf('lengths: \n train = %.3f, test = %.3f\n',...
        mean_train_lengths, mean_test_lengths);
end

subfolder = '_speeds';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_speeds = data.wrong_labeled_percent{ind_designers_test};
    mean_train_speeds = mean(wrong_labeled_percent_speeds(objects_numbers_train_order));
    mean_test_speeds  = mean(wrong_labeled_percent_speeds(objects_numbers_test_order));
    fprintf('speeds:\n train = %.3f, test = %.3f\n',...
        mean_train_speeds, mean_test_speeds);
end

subfolder = '_stroke_numbers';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_stroke_numbers = data.wrong_labeled_percent{ind_designers_test};
    mean_train_stroke_numbers = mean(wrong_labeled_percent_stroke_numbers(objects_numbers_train_order));
    mean_test_stroke_numbers  = mean(wrong_labeled_percent_stroke_numbers(objects_numbers_test_order));
    fprintf('stroke_numbers: \n train = %.3f, test = %.3f\n',...
        mean_train_stroke_numbers, mean_test_stroke_numbers);
end

subfolder = '_brightness_stroke_numbers_speeds_curvatures';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))

    data = load(filename);
    wrong_labeled_percent_bsnsc = data.wrong_labeled_percent{ind_designers_test};
    mean_train_brightness_stroke_numbers_speeds_curvatures = ...
        mean(wrong_labeled_percent_bsnsc(objects_numbers_train_order));
    mean_test_brightness_stroke_numbers_speeds_curvatures  = ...
        mean(wrong_labeled_percent_bsnsc(objects_numbers_test_order));
    fprintf('brightness_stroke_numbers_speeds_curvatures: \n train = %.3f, test = %.3f\n',...
    mean_train_brightness_stroke_numbers_speeds_curvatures, ...
    mean_test_brightness_stroke_numbers_speeds_curvatures);
end

subfolder = '_brightness_stroke_numbers_speeds';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_bsns = data.wrong_labeled_percent{ind_designers_test};
    mean_train_brightness_stroke_numbers_speeds = ...
        mean(wrong_labeled_percent_bsns(objects_numbers_train_order));
    mean_test_brightness_stroke_numbers_speeds  = ...
        mean(wrong_labeled_percent_bsns(objects_numbers_test_order));
    fprintf('brightness_stroke_numbers_speeds: \n train = %.3f, test = %.3f\n',...
        mean_train_brightness_stroke_numbers_speeds, ...
        mean_test_brightness_stroke_numbers_speeds);
end

subfolder = '_brightness_stroke_numbers_speeds_lengths';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_bsnsl = data.wrong_labeled_percent{ind_designers_test};
    mean_train_brightness_stroke_numbers_speeds_lengths = ...
        mean(wrong_labeled_percent_bsnsl(objects_numbers_train_order));
    mean_test_brightness_stroke_numbers_speeds_lengths  = ...
        mean(wrong_labeled_percent_bsnsl(objects_numbers_test_order));
    fprintf('brightness_stroke_numbers_speeds_lengths: \n train = %.3f, test = %.3f\n',...
        mean_train_brightness_stroke_numbers_speeds_lengths, ...
        mean_test_brightness_stroke_numbers_speeds_lengths);
end

subfolder = '_brightness_stroke_numbers_speeds_lengths_curvatures';
filename = fullfile(folder, subfolder, 'classifier.mat');
if (exist(filename, 'file'))
    data = load(filename);
    wrong_labeled_percent_bsnslc = data.wrong_labeled_percent{ind_designers_test};
    mean_train_brightness_stroke_numbers_speeds_lengths_c = ...
        mean(wrong_labeled_percent_bsnslc(objects_numbers_train_order));
    mean_test_brightness_stroke_numbers_speeds_lengths_c  = ...
        mean(wrong_labeled_percent_bsnslc(objects_numbers_test_order));
    fprintf('brightness_stroke_numbers_speeds_lengths_curvatures: \n train = %.3f, test = %.3f\n',...
    mean_train_brightness_stroke_numbers_speeds_lengths_c, ...
    mean_test_brightness_stroke_numbers_speeds_lengths_c);
end
% 
% fprintf('all: \n train = %.3f, test = %.3f\n', ...
%     mean_train_all, mean_test_all)
row_f{1,1} = designer.id;
row{1,1} = 1.0 - mean_train_brightness;
row{1,2} = 1.0 - mean_test_brightness;
row{1,3} = 1.0 - mean_train_curvature;
row{1,4} = 1.0 - mean_test_curvature;
row{1,5} = 1.0 - mean_train_lengths;
row{1,6} = 1.0 - mean_test_lengths;
row{1,7} = 1.0 - mean_train_speeds;
row{1,8} = 1.0 - mean_test_speeds;
row{1,9} = 1.0 - mean_train_stroke_numbers;
row{1,10} = 1.0 - mean_test_stroke_numbers;
row{1,11} = 1.0 - mean_train_brightness_stroke_numbers_speeds;
row{1,12} = 1.0 - mean_test_brightness_stroke_numbers_speeds;
row{1,13} = 1.0 - mean_train_brightness_stroke_numbers_speeds_curvatures;
row{1,14} = 1.0 - mean_test_brightness_stroke_numbers_speeds_curvatures;
row{1,15} = 1.0 - mean_train_brightness_stroke_numbers_speeds_lengths;
row{1,16} = 1.0 - mean_test_brightness_stroke_numbers_speeds_lengths;
row{1,17} = 1.0 - mean_train_brightness_stroke_numbers_speeds_lengths_c;
row{1,18} = 1.0 - mean_test_brightness_stroke_numbers_speeds_lengths_c;
row_f(1,2:19) =row;
end