function trainAndValidateOneDesigner(designers, designers_train_indices, ...
                                     designers_str,...
                                        objects_numbers_train_designers, features)


% folder = fullfile('./results', designer.id);
folder = fullfile('./results', designers_str);
subfolder = '';
for i = 1:size(features,2)
    subfolder = [subfolder '_' features{i}];
end

folder = fullfile(folder, subfolder);

if ~exist(folder, 'dir')
    mkdir(folder)
end

filename = fullfile(folder, 'classifier.mat');


if ~exist(filename, 'file')

    [svmmod, X, Y] = ...
        trainClassifier(designers, designers_train_indices, objects_numbers_train_designers, features);

    save(filename, ...
     'svmmod', 'X', 'Y', 'designers_train_indices');
else
    load(filename, ...
         'svmmod', 'X', 'Y');
    save(filename, ...
        'svmmod', 'X', 'Y', 'designers_train_indices');
%      'svmmod', 'X', 'Y', 'objects_numbers_train', 'objects_numbers_test', 'designer');
 
end

wrong_labeled_percent           = cell(15, 1);
wrong_labeled_construction      = cell(15, 1);
wrong_labeled_descriptive       = cell(15, 1);
for ind_designer = 1:15
    [ labels{ind_designer},...
      wrong_labeled_percent{ind_designer}, ...
      wrong_labeled_construction{ind_designer}, ...
      wrong_labeled_descriptive{ind_designer}] = visualizeTrainigResults(svmmod, designers(ind_designer), features, folder);
end

save(filename, ...            
            'wrong_labeled_percent', ...
            'wrong_labeled_construction', ...
            'wrong_labeled_descriptive', '-append');
end